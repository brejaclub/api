## Descrição

API do Brejaclub.

### Estrutura

- public/
- scripts/
- src/
   - /controllers
   - /helpers
   - /middlewares
   - /models
   - /routes
- tests